#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;
#define TRZSIZE 16
int table[TRZSIZE][TRZSIZE][70000];
bool input[TRZSIZE][TRZSIZE];
int I, J;
#define MODER 1000000007
void process()
{
  scanf("%d %d", &I, &J);
  for (int i(0); i < I; ++i)
  {
    char str[30];
    scanf("%s", str);
    for (int j(0); j < J; ++j)
      input[i][j] = ('X' == str[j]);// 1 = occupied
  }
  for (int i(0); i < I; ++i)
    input[i][J] = 1; 
  for (int j(0); j < J; ++j)
    input[I][j] = 1;

  memset(table, 0, sizeof(table));	//initialization
  
  int K_MAX = 1 << J;
  /*
  for (int i(0); i <= I; ++i)
    for (int j(0); j <= J; ++j)
      for (int k(0); k <= K_MAX; ++k)
	table[i][j][k] = 0;
  */
  table[0][0][0] = 1;

  int acpt = (1<<J)-1;//11111111, len = J-1
  int Lc1_cmpr(1<<(J-1)), Lc2_cmpr(1<<(J-2));
  int *ptr; 
  for (int i(0), j(0); i != I;)
  {
    bool boundary = (j == J-1);
    int Nj = (boundary)? 0: j+1, Ni = (boundary)? i+1: i;
    bool Nboundary = (Nj == J-1);
    int NNj = (Nboundary)? 0: Nj+1, NNi = (Nboundary)? Ni+1: Ni;	
    for (int k(0); k < K_MAX; ++k)
    {
      int sum = table[i][j][k];
      if (!sum) continue;
      //printf("[%d][%d][%d] = %d\n", i, j, k, sum);
      bool CUR_ok = !(input[i][j]) && !(k & Lc1_cmpr),
	   R_ok = !(input[i][j+1]) && !(k & Lc2_cmpr),
	   D_ok = !(input[i+1][j]),
	   RD_ok = !(input[i+1][j+1]);
      ptr = &table[Ni][Nj][ (k<<1) & acpt];
      *ptr += sum;
      *ptr %= MODER;
      if (CUR_ok && R_ok) 
      {
	ptr = &table[NNi][NNj][ (k<<2) & acpt];
	*ptr += sum;
	*ptr %= MODER;
      }
      if (CUR_ok && D_ok)
      {
	ptr = &table[Ni][Nj][( (k<<1)|(1) ) & acpt];
	*ptr += sum;
	*ptr %= MODER;
      }
      if (CUR_ok && R_ok && D_ok && RD_ok)
      {
	ptr = &table[NNi][NNj][( (k<<2)|(3) )];
	*ptr += sum;
	*ptr %= MODER;
      }
    }
    i = Ni;
    j = Nj;
  }
  printf("%d\n", table[I][0][0]);
}
int main()
{
  int testCase;
  scanf("%d", &testCase);
  for (int i(0); i < testCase; ++i)
  {
    process();  
  }
  return 0;
}

//
