#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
using ULL = unsigned long long;//ULL trap

struct Box
{
  ULL longer, shorter;
  void init(const ULL& A, const ULL& B)
  {  
    longer = (A > B)? A:B;
    shorter = (A < B)? A:B;
  }
  bool same(const Box& b)
  {  return (longer==b.longer)&&(shorter==b.shorter);}
};
bool boxCompare(const Box& a,const Box& b)
{
  if (a.longer == b.longer)  
    return a.shorter < b.shorter;
  return a.longer < b.longer;
}
class Problem
{
  public:
    Problem(vector<Box>& B, const ULL& l, const ULL& r):
      base(B), L(l), R(r){}
    ULL answer()
    {
      if (baseCase())  return 0;
      ULL mid = (L+R)/2;
      Problem LP(base, L, mid), RP(base, mid, R);
      return LP.answer() + RP.answer() + cross(LP, RP);
    }
  private:
    vector<Box>& base;
    ULL L, R;
  private:
    bool baseCase(){ return (R-L)==1;}
    ULL cross(Problem& LP, Problem& RP)
    {
      vector<Box> tmpVec;
      ULL sum(0), l(LP.L), r(LP.R);//iterator
      while ( (l!=LP.R) && (r!=RP.R) )
      {
	if (base[l].shorter <= base[r].shorter)
	{
	  sum += (RP.R - r);
	  for(ULL tmp(r);  //check the same
	      (tmp != RP.R) && (base[l].same(base[tmp]));
	      ++tmp) ++sum;
	  tmpVec.push_back(base[l++]);
	}
	else
	  tmpVec.push_back(base[r++]);
      }
      while(LP.R != l) tmpVec.push_back(base[l++]);
      while(RP.R != r) tmpVec.push_back(base[r++]);
      copy(tmpVec.begin(), tmpVec.end(), base.begin() + LP.L);
      return sum;
    }
};
void process(ULL& elts)
{
  vector<Box> BoxArray;
  BoxArray.resize(elts);
  for (ULL i(0); i < elts; ++i)
  {
    ULL A, B;
    scanf("%lld %lld", &A, &B);
    BoxArray[i].init(A, B);
  }
  sort(BoxArray.begin(), BoxArray.end(), boxCompare);
  printf("%lld\n", Problem(BoxArray, 0, elts).answer());
  return;
}
int main()
{
  ULL times;
  scanf("%lld", &times);
  for(ULL i(0); i < times; ++i)
  {
    ULL elements;
    scanf("%lld", &elements);
    process(elements);
  }
  return 0;
} 

