#include <iostream>
#include <cstdio>
#include <list>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

struct Node;
struct Edge
{
  Node &A, &B;
  bool color;
  int weight;
  Edge(Node &a, Node &b):A(a), B(b), color(false){}
};
struct Node
{
  list<Edge*> _list;
  bool color;
  void push_back(Edge &e){_list.push_back(&e);}
  Node& toNode(Edge& e)
  {// e belongs to _list
    return (&e.A != this)? e.A: e.B; 
  }
};
struct ConnectChecker
{
  int counter;
  ConnectChecker(): counter(0){}
  void whitening(vector<Node>&);
  void DFS(Node&);
  bool answer();
};
inline void ConnectChecker::whitening(vector<Node>& _nodes)
{
  counter = _nodes.size();
  for (auto &node: _nodes)
    node.color = false;
}
void ConnectChecker::DFS(Node& anyNode)
{
  anyNode.color = true;//white to black;
  --counter;
  for (auto* edge : anyNode._list)
  {
    if (edge->color == true) continue;
    Node& next = anyNode.toNode(*edge);
    if (next.color != true )
      DFS(next);
  }
}
inline bool ConnectChecker::answer()
{  return counter == 0;}

struct Graph
{
  //map<int, Node> _nodes;
  vector<Node> _nodes;
  list<Edge> _edges;
  Graph(int ns)
  {
    _nodes.resize(ns);
  }
  Edge& make_edge(Node& a, Node & b)
  {
    _edges.push_back(Edge(a, b));
    Edge &e( _edges.back() );
    a.push_back(e);
    b.push_back(e);
    return e;
  }
  bool connected()
  {
    ConnectChecker c;
    c.whitening(_nodes);
    c.DFS(_nodes[0]);
    return c.answer();
  }
};
struct Mask
{
  list<Edge*> edges;  
  void tag(Edge &e){ e.color = true; edges.push_back(&e);}
  inline void resume()
  {
    for (auto *edge: edges) 
      edge->color = false;
  }
};
void main_process(Graph &graph)
{
  int ans(0);
  for (int bit(31); bit >= 0; --bit)
  {
    int bitChecker(1 << bit);
    Mask mask;
    for (auto &edge : graph._edges)
    {
      if ((edge.color == false) && ((bitChecker&edge.weight)> 0))
	mask.tag(edge);
    }
    if (!graph.connected())
    {
      //printf("disconnected at %d bit\n", bit);
      mask.resume();
      ans |= bitChecker;
    }
  }
  printf("%d\n", ans);
}
int main()
{
  int testcase;
  scanf("%d", &testcase);
  while(testcase--)
  {
    int nodes, edges;
    scanf("%d %d", &nodes, &edges);
    Graph graph(nodes);
    for (int i(0); i < edges; ++i)
    {
      int numA, numB, weight;
      scanf("%d %d %d", &numA, &numB, &weight);
      Node &nodeA(graph._nodes[numA-1]), &nodeB(graph._nodes[numB-1]);
      graph.make_edge(nodeA, nodeB).weight = weight;
    }
    main_process(graph);
  }
}

