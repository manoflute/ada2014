#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
int maxlen[2345][2345];
int Atable[2345][26];
int Btable[2345][26];
void process(char *A, char *B)
{
  int len_A(strlen(A)), len_B(strlen(B));
  for (int a = len_A; a >= 0; --a)
    maxlen[a][len_B] = 0;
  for (int b = len_B; b >= 0; --b)
    maxlen[len_A][b] = 0;
  for (int a = len_A-1; a >= 0; --a)
    for (int b = len_B-1; b >= 0; --b)
    {
      char charA(A[a]), charB(B[b]);
      int &curVal(maxlen[a][b])
	, Rval(maxlen[a][b+1])
	, Dval(maxlen[a+1][b])
	, RDval(maxlen[a+1][b+1]);
      curVal = (charA == charB)? RDval + 1 :
	(Rval > Dval)? Rval: Dval; 
    }
  for (int al(0); al < 26; ++al)
  {
    char alpha('a'+al);
    Atable[len_A][al] = Btable[len_B][al] = -1;
    for (int i(len_A-1); i >= 0; --i)
      Atable[i][al] = (alpha == A[i])? i : Atable[i+1][al];
    for (int i(len_B-1); i >= 0; --i)
      Btable[i][al] = (alpha == B[i])? i : Btable[i+1][al];  
  }
  int goal = maxlen[0][0];
  int a_len(0), b_len(0);
  while(0 != goal)
    for (int i(0); i < 26; ++i)
    {
      int firstPos_a(Atable[a_len][i]);
      int firstPos_b(Btable[b_len][i]);
      if (goal == maxlen[firstPos_a][firstPos_b])
      {
	putchar(i+'a');
	a_len = firstPos_a + 1;
	b_len = firstPos_b + 1;
	--goal;
	break;
      }
    }
  printf("\n"); 
}

int main()
{
  int testCase;
  scanf("%d", &testCase);
  for (int i(0); i < testCase; ++i)
  {
    char A[2345], B[2345];
    scanf("%s %s", A, B);
    process(A, B);
  }
  return 0;
}

