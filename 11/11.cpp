#include <iostream>
#include <cstdio>
#include <algorithm>
#include <limits.h>
#include <vector>
#define int128 __uint128_t
using namespace std;
int128 adj[128];    // adjacency matrix
int N;
const int128 one = 1;
const int128 zero = 0;
int size_clique;
int ctz(int128 target)
{
  long long tmp = target;
  if (!tmp) return __builtin_ctzll((long long)(target>>64))+64;
  else return __builtin_ctzll(tmp);
}
int count(int128 target)
{
  return __builtin_popcountll((long long)target)
    + __builtin_popcountll((long long)(target>>64));
}
void backtrack(int size_R, int128 P, int128 X)
{
  //cut branch
  if (size_R + count(P) <= size_clique) return;
  //
  if (!P) 
  {
    if (!X){size_clique = max(size_clique, size_R);}
    return;
  } 
  int128 p = ctz(P | X );
  int128 Q = P & ~adj[p];
  while (Q)
  {
    int i = ctz(Q);
    if(i == p||Q & adj[i])
      backtrack(size_R+1, P & adj[i], X & adj[i]);
    P &= ~(one<<i);
    X |= (one<<i);
    Q &= ~(one<<i);
  }
}

void Bron_Kerbosch()
{
  // remove loop
  size_clique = 0;
  for (int i=0; i<N; ++i)
    adj[i] &= ~(one<<i); 
  backtrack(zero, (one<<N)-one, zero);
  printf("%d\n",size_clique);
}
void process()
{ Bron_Kerbosch();}
int compare(const void *a, const void *b)
{
  //return (**(int**)b)-(**(int**)a);
  return -(**(int**)a)-(**(int**)b);
}
int main()
{
  int testCase;
  scanf("%d", &testCase);
  while(testCase--)
  {
    int E;
    scanf("%d%d", &N, &E);
    int degree_base[128] = {};
    int *degree[128];
    vector<pair<int,int> > input;
    input.reserve(N);
    for (int i(0); i < N; ++i)
    { degree[i] = degree_base+i;}
    for(int e(0); e < E; ++e)
    {
      int a, b;
      scanf("%d%d", &a, &b);
      input.push_back(make_pair(a,b));
      ++degree_base[a]; ++degree_base[b];
    }
    qsort(degree, N, sizeof(int*), compare);
    int toNewPos[128];
    for (int i(0); i < N; ++i)
    {
      toNewPos[(degree[i])-degree_base] = i;
      //printf("%d(%d) -> %d\n", (degree[i])-degree_base, **degree, i);
    }

    for (int i(0); i < N; ++i) 
      adj[i] = (one<<N)-one;
    for (; !input.empty(); input.pop_back())
    {
      int a(input.back().first), b(input.back().second);
      
      adj[toNewPos[b]] &= ~(one<<toNewPos[a]);
      adj[toNewPos[a]] &= ~(one<<toNewPos[b]);
    }
    process();
  }
  return 0;
}

