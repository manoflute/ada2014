#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
#define MAXSIZE 222222
int Cpos[MAXSIZE];
int cities;
int ports;
int dotsLeftAtDist(int dist)
{
  int coveringZone(-1);
  int *CposEnd(Cpos+cities), *pCity(Cpos);
  int left(ports);//left dots at dist
  while (pCity != CposEnd) 
  {
    if (*pCity <= coveringZone)
      ++pCity;
    else
    {
      int *dottedNext(CposEnd);
      for (int *dotting(pCity+1); 
	  dotting != CposEnd && dottedNext == CposEnd; ++dotting)
      {
        if (*dotting - *pCity > dist)//the range is over distance
          dottedNext = dotting;//label it
      }
      --left;//!!do not need to check totalLength
      coveringZone = *(dottedNext-1) + dist;
      pCity = dottedNext;
    }
  }
  return left;
}
int greedy()
{
  sort(Cpos, Cpos+cities);
  int Up = (Cpos[cities-1]-Cpos[0]); //upper bound
  int Low = 0; //lower bound 
  //printf("testing = %d\n",dotsLeftAtDist(Cpos, N, dots, 4));
  while (Up>Low)
  {
    int Mid = Low + (Up - Low) / 2;
    int flag = dotsLeftAtDist(Mid);
    //printf("(%d)~%d~(%d) = %d\n", Up, Mid, Low, flag);
    if (flag >= 0)
    { Up = Mid;}//downward approaching
    else if (flag < 0)
    { Low = Mid+1;}//upward approaching
  }
  int OPT(Low);
  return OPT;
}
int main()
{
  int testCase;
  scanf("%d", &testCase);
  while (testCase--)
  {
    scanf("%d%d", &cities, &ports);
    for (int i(0); i < cities; ++i)
      scanf("%d", &Cpos[i]);
    printf("%d\n", greedy());
  }
  return 0;
}

