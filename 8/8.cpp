#include <iostream>
#include <cstdio>
#include <list>
#include <algorithm>
//#include <windows.h>
using namespace std;
enum Color {NEW = 0, VISITED, CLOSED};
enum Symbol {EMPTY = '.', TRAP = '#', START = 'S', END = 'E', BEVERAGE = 'B'};
struct PInfo
{
  char sym, color;
  int dist, beverage;
  PInfo():sym(0), color(NEW), dist(0), beverage(0){}
  inline bool isSym(char c){ return (sym == c);} 
  inline bool isClr(char c){ return (color == c);}
};
struct Point
{
  int x, y, z; 
  Point(int a=0, int b=0, int c=0):x(a),y(b),z(c){} 
  inline Point mov(int a, int b, int c){ return Point(x+a, y+b, z+c);}
};
PInfo gval[105][105][105];
struct Graph
{ 
  PInfo (*val)[105][105]; 
  int size;
  Point start, end;
  Graph():val(gval){}
  //inline PInfo& elt(int a, int b, int c){ return val[a][b][c];}
  inline PInfo& elt(Point& p){ return val[p.x][p.y][p.z];}
  bool legal(Point& p)
  {
    int arr[] = {p.x, p.y, p.z};
    bool ans(true);
    for (int i(0); ans && i < 3; ++i)
      ans = (arr[i] >= 0) && (arr[i] < size);    
    return ans;
  }
};
void main_process(Graph g)
{
  list<Point> Queue;
  Queue.push_back(g.start);
  bool queueOpen = true;
  while (!Queue.empty())
  {
    Point tmp = Queue.front();
    Queue.pop_front();
    //Sleep(100); 
    PInfo& tmpInfo( g.elt(tmp) );
    Point pt[6] = {tmp.mov(1,0,0), tmp.mov(-1,0,0), tmp.mov(0,1,0),
      tmp.mov(0,-1,0), tmp.mov(0,0,1), tmp.mov(0,0,-1)};
    for (int i(0); i < 6; ++i)
    {
      Point& curPt( pt[i] );
      PInfo& curInfo( g.elt(curPt) );
      if (!g.legal(curPt)) continue;//legal position
      if (curInfo.isSym(TRAP)){ curInfo.color = CLOSED;continue;}
      //seperated condition
      if (curInfo.isClr(NEW))
      {
        curInfo.color = VISITED;
	curInfo.dist = 1 + tmpInfo.dist;
	curInfo.beverage = (curInfo.isSym(BEVERAGE))? 
	  1 + tmpInfo.beverage: tmpInfo.beverage;
	if (queueOpen && curInfo.isSym(END)) queueOpen = false;
	if (queueOpen) Queue.push_back(curPt);
      }
      else if (curInfo.isClr(VISITED) && (curInfo.dist == tmpInfo.dist + 1))
      {
        int new_bev = (curInfo.isSym(BEVERAGE))? 
	  1 + tmpInfo.beverage: tmpInfo.beverage;
	curInfo.dist = 1 + tmpInfo.dist;
	curInfo.beverage = max(curInfo.beverage, new_bev);
      }
    }
    tmpInfo.color = CLOSED;
  }

  //output
  PInfo& endInfo = g.elt(g.end);
  if (endInfo.color == NEW)
    printf("Fail OAQ\n");
  else 
    printf("%d %d\n", endInfo.dist, endInfo.beverage);
}
int main()
{
  int T;
  scanf("%d", &T);
  while (T--)
  { 
    Graph g;
    scanf("%d", &g.size);
    int size = g.size;
    for (int i(0); i < size; ++i)
      for (int j(0); j < size; ++j)
      {
	char buffer[120];
	scanf("%s", buffer);
	for (int k(0); k < size; ++k)
	{
	  Point curPoint(i, j, k);
	  PInfo curInfo;
	  char sym = curInfo.sym = buffer[k];
	  if (sym == START){  g.start = curPoint;}
	  else if (sym == END){ g.end = curPoint;}
	  g.elt(curPoint) = curInfo;
	}
      } 
    g.elt(g.start).color = VISITED;
    main_process(g);
  }
}
