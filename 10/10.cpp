#include <iostream>
#include <cstdio>
#include <list>
#include <algorithm>
#define MAXSIZE 1000000
using namespace std;
struct Problem
{
  int *intels;
  int size;
  int *answers;
  Problem(int *i, int s, int *a):intels(i), size(s), answers(a){}
  void getAsked()
  {
    list<int> indexStack;
    int index_max = max_element(intels, intels+size) - intels;
    indexStack.push_back(index_max);
    //printf("max = intels[%d] = %d\n", index_max, intels[index_max]);
    for (int i(circle(index_max-1)); i != index_max; i=circle(i-1))
    {    
      for (; bigger(i, indexStack.back()); indexStack.pop_back())
      { answers[indexStack.back()] = i;} 
      indexStack.push_back(i);
      //printf("i = %d\n", i);
    }
    //printf("finish1 (%d)\n", indexStack.empty());
    int i_largest = indexStack.front();
    for(;!indexStack.empty() && intels[i_largest]==intels[indexStack.front()]; 
	indexStack.pop_front())
    { answers[indexStack.front()] = -1;}
    for(;!indexStack.empty(); indexStack.pop_front())
    { answers[indexStack.front()] = i_largest;} 
  }
  bool sameValue(int A, int B){ return intels[A]==intels[B];}
  bool bigger(int A, int B){ return intels[A]>intels[B];}
  int circle(int A){ return (A+size)%size;}
};
int intelligence[MAXSIZE];
int answers[MAXSIZE];
int main()
{
  int testCase;
  scanf("%d", &testCase);
  while(testCase--)
  {
    int n;
    scanf("%d", &n);

    for (int i(0); i < n; ++i)
      scanf("%d", &intelligence[i]);
    Problem(intelligence, n, answers).getAsked();
    for (int i(0); i < n; ++i)
      printf("%d%c", answers[i]+1,(i!=n-1)?' ':'\n');
  }
  return 0;
}
