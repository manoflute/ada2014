#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;
int p, c[10];//1,5,10,20,50,100,200,500,1000,2000
int money[10]={1,5,10,20,50,100,200,500,1000,2000};
int greedy()
{
  int clerkP(-p);
  int totalCoin(0);
  for (int i(0); i < 10; ++i)//get clerkP & totalMoney
  {
    clerkP += c[i] * money[i];
    totalCoin += c[i];
  }
  if (clerkP < 0) return -1;//no money to pay
  
  int minCoin(2147483647);
  int tmp_clerkP(clerkP);
  for (int code(0); code < 64; ++code)
  {
    clerkP = tmp_clerkP;//initialization
    int clerk_c[10] = {0};//initialization
    int cond = code << 4;//generalize code to all 0~9
    for (int i(9); i >= 0; --i)//solution
    {
      if (clerkP >= money[i] && c[i] > 0)//normal solution
      {
	int coins = min(c[i], clerkP/money[i]);
	clerkP -= coins * money[i];
	clerk_c[i] += coins;
      }
      if (((cond >> i)&1) && clerk_c[i] > 0)//the -1 condition
      {
	clerkP += money[i];//return money
        --clerk_c[i];//return coin
      }
    }
    int coin(0);
    //printf("clerkP = %d\n", clerkP);
    for (int i(0); i < 10; ++i)//get coin
    { 
      coin += clerk_c[i]; 
    }
    if (clerkP>0) continue;//left money
    minCoin = min(minCoin, coin);
  }
  return (minCoin==2147483647)? -1: totalCoin-minCoin;
}
int main()
{
  int testCase;
  scanf("%d", &testCase);
  while (testCase--)
  {
    scanf("%d", &p);
    for (int i(0); i < 10; ++i)
      scanf("%d", &c[i]);
    printf("%d\n", greedy());
  }
  return 0;
}

